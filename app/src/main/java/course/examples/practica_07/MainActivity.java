package course.examples.practica_07;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TabHost tabH = (TabHost)findViewById(R.id.tabHost);
        tabH.setup();

        TabHost.TabSpec tab1 = tabH.newTabSpec("tab1");
        TabHost.TabSpec tab2 = tabH.newTabSpec("tab2");

        tab1.setIndicator("LLamada");
        tab1.setContent(R.id.tab1);

        tab2.setIndicator("Mensaje");
        tab2.setContent(R.id.tab2);

        tabH.addTab(tab1);
        tabH.addTab(tab2);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void call(View view){
        EditText ed = (EditText)findViewById(R.id.telefono);
        String tel = "tel:" + ed.getText().toString();
        try{
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse(tel));
            startActivity(callIntent);
        }catch (ActivityNotFoundException activityException){
            Log.e("dialing-example", "Call failed", activityException);
        }catch (SecurityException secEx){
            Log.e("dialing-example", "Call failed", secEx);
        }
    }

    public void enviarMensaje(View vw){
        EditText edtxt = (EditText)findViewById(R.id.telMsj);
        String tel = edtxt.getText().toString();
        edtxt = (EditText)findViewById(R.id.msj);
        String mensaje =edtxt.getText().toString();

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(tel,null,mensaje,null,null);
        Toast.makeText(getBaseContext(),"Mensaje Enviado!",Toast.LENGTH_LONG);
    }



}
